.. _tutorials:

Tutorials
=========

This page contains tutorials for using
the engine and modifying the game.

.. toctree::
	:maxdepth: 1

	tuts_custom-maps
	tuts_enemy-pathfinding
